import UIKit

@IBDesignable
class RingTimerView: UIView {
    @IBInspectable var lineWidth: CGFloat = 25 // FIXME: percentage of the rect size by default
    @IBInspectable var ringBackgroundColor: UIColor = UIColor.lightGray
    @IBInspectable var ringForegroundColor: UIColor = UIColor.blue // FIXME: default to tintColor
    @IBInspectable var timeVisible: Bool = true

    override func draw(_ rect: CGRect) {
        super.draw(rect)

        drawRing(percent: 1, color: ringBackgroundColor, rect: rect)
        drawRing(percent: 0.55, color: tintColor, rect: rect)

        if timeVisible {
            let label = UILabel(frame: rect)
            label.text = "00:00:00"
            label.textAlignment = NSTextAlignment.center
            label.textColor = tintColor
            label.font = UIFont.systemFont(ofSize: 36)
            label.adjustsFontSizeToFitWidth = true
            self.addSubview(label)
        }
    }

    private func drawRing(percent: CGFloat, color: UIColor, rect: CGRect) {
        let centerX = rect.maxX / 2
        let centerY = rect.maxY / 2

        let smallestCenter = centerX < centerY ? centerX : centerY
        let radius = smallestCenter - (lineWidth / 2)

        let startAngle = CGFloat(-Double.pi / 2)
        let endAngle = startAngle + 2 * CGFloat(CGFloat(Double.pi) * percent)

        let circlePath = UIBezierPath(arcCenter: CGPoint(x: centerX, y: centerY),
                                      radius: radius,
                                      startAngle: startAngle,
                                      endAngle: endAngle,
                                      clockwise: true)
        color.setStroke()
        circlePath.lineWidth = lineWidth
        circlePath.stroke()
    }
}
